var server = window.location.hostname;

if (server == 'localhost' || server == 'dev.codekernal.com') {
    var canvasUrl = location.protocol + "//" + server + "/practice/public/";
} 
else {
    var canvasUrl = location.protocol + "//"+server+"/";
}


var config = {};

// Front app url
config.frontUrl = canvasUrl;

// API url
config.frontApiURL = canvasUrl + "api/";

config.adminUrl = canvasUrl + 'admin';

// API url
config.adminApiURL = canvasUrl + "admin/api/";


function c(value)
{
	console.log(value);
}