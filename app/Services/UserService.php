<?php

namespace App\Services;
use App\Models\User;


class UserService
{
    public function register($request)
    {
        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->save();

        return $user->id;
    }
}
