<?php

namespace App\Http\Controllers\Front;


use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Services\UserService;


class UserController extends Controller
{
	public $service;

	function __construct(UserService $userService)
	{
		$this->service = $userService;
	}

   public function showRegister()
   {
       return view('front.register');
   }


   public function register(Request $request)
   {
   		//print_r($request->all());
   		//echo $request->input('name');

   		$resp = $this->service->register($request);


   		if(!empty($resp))
   		{
			return response()->json([
				    'status' => 'success',
				    'message' => 'User registered'
				], 200);   			
   		}
   		else
   		{
			return response()->json([
				    'status' => 'error',
				    'message' => 'Some error'
				], 401);   			   			
   		}


   }

}
